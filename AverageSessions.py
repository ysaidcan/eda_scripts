import pandas as pd

# read csv file
df = pd.read_csv('./son.csv')

# perform groupby
res = df.groupby(['SessionID'])[['Mean', 'Peak','Perc20','Perc80','QuartDev','Std','Strong_Peak','dataQual','Max','Min',
                                 'SessionLabel','SessionLabel2','Device','Lab_sub', 'derivative']].mean().reset_index()
#res = df.groupby(['device','label'])[['fv_e1', 'fv_e2','fv_e3','fv_e4','fv_e5','fv_e6','fv_e7','fv_e8','fv_e9','fv_e10','fv_e11', 'fv_e12','fv_e14','fv_e15','fv_e16','fv_e17','class']].mean().reset_index()
#res = df.groupby(['device','label'])[['stepCount','stillness']].mean().reset_index()
print(res)

#    Cluster   Latitude  Longitude
# 0      214  22.289210  19.812105
# 1      422  22.332293  19.752901
# 2      500  22.296393  19.624689

# write to csv
res.to_csv('Ali_label_avg.csv', index=False)