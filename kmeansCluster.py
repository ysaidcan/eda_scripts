from copy import deepcopy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)
plt.rcParams['figure.figsize'] = (16, 9)

data=pd.read_csv('peakProps.csv')

f1 = data['mean'].values
f2 = data['std'].values
f3 = data['peak'].values
f4 = data['session'].values
f5 = data['label_subjective'].values
data=(f1,f2,f3)
colors = ("red", "green", "blue")
groups = ("mean", "std", "peak") 
X = np.array(list(zip(f1, f2, f3)))
Subjective_Labels=np.array(list(zip(f5)))




# Number of clusters
k = 2
Number_of_features=3
from sklearn.cluster import KMeans

kmeans = KMeans(n_clusters=k)
kmeans.fit(X)
a=kmeans.cluster_centers_
y_kmeans = kmeans.predict(X)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, axisbg="1.0")
ax = fig.gca(projection='3d')

unique, counts = np.unique(y_kmeans, return_counts=True)
dictionary=dict(zip(unique, counts))
shape1=(dictionary[1],Number_of_features)
shape2=(dictionary[0],Number_of_features)
if(k==3):
    shape3=(dictionary[2],Number_of_features)
    
x1=np.zeros(shape1)
x1_label=np.zeros(shape1)
x2=np.zeros(shape2)
x2_label=np.zeros(shape2)
if(k==3):
    x3=np.zeros(shape3)
    x3_label=np.zeros(shape3)
count1=0
count2=0
if(k==3):
    count3=0
for i in range(0,len(X)):
    if(y_kmeans[i]==0):
        x2[count1,:]=X[i]
        x2_label[count1,:]=Subjective_Labels[i]
        count1=count1+1
    elif(y_kmeans[i]==1):
        x1[count2,:]=X[i]
        x1_label[count2,:]=Subjective_Labels[i]
        count2=count2+1
    else:
        if(k==3):
            x3[count3,:]=X[i]
            x3_label[count3,:]=Subjective_Labels[i]
            count3=count3+1
    
#ax.scatter(f1, f2, f3, alpha=0.8, c='r',  edgecolors='none')
for i in range(len(x1)):            
    ax.scatter(x1[i,0],x1[i,1],x1[i,2],c='b')
    ax.text(x1[i,0],x1[i,1],x1[i,2],  '%s' % (str(x1_label[i,0])), size=10, zorder=1, color='k') 
for i in range(len(x2)):  
    ax.scatter(x2[i,0],x2[i,1],x2[i,2],c='r',marker='x')
    ax.text(x2[i,0],x2[i,1],x2[i,2],  '%s' % (str(x2_label[i,0])), size=10, zorder=1, color='k') 
if(k==3):
    for i in range(len(x3)): 
        ax.scatter(x3[i,0],x3[i,1],x3[i,2],c='g',marker='^')
        ax.text(x3[i,0],x3[i,1],x3[i,2],  '%s' % (str(x3_label[i,0])), size=10, zorder=1, color='k') 
    

 
plt.title('Matplot 3d scatter plot')
plt.legend(loc=2)
plt.show()