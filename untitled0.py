#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 23:03:05 2018

@author: root
"""


import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score




# Use only one feature
diabetes_y_train =[ 34.4, 41.7, 46.6, 38.8, 49.9, 44.1, 40.9, 42.56]
diabetes_y_train1 =[ 7.3, 4.9, -7.8, 11.1, -5.8, -3.2, 1.7] #oy
diabetes_X_train1 = [ 6.5, 5.9, -6.9, 10.3, 4.5, 2.1, 8.6] #growth
diabetes_X_train = [ 4.3, 6.5, 5.9, -6.9, 10.3, 4.5, 2.1, 8.6]

mycorr=np.corrcoef(diabetes_y_train1, diabetes_X_train1)
corr=np.corrcoef(diabetes_y_train, diabetes_X_train)
diabetes_y_train=np.array(diabetes_y_train)
diabetes_y_train=diabetes_y_train.reshape(-1, 1)

diabetes_y_train1=np.array(diabetes_y_train1)
diabetes_y_train1=diabetes_y_train1.reshape(-1, 1)

diabetes_y_test=35

# Split the data into training/testing sets
diabetes_X_test = -3
diabetes_X_test=np.array(diabetes_X_test)
diabetes_X_test=diabetes_X_test.reshape(1, -1)

# Split the targets into training/testing sets

diabetes_X_train=np.array(diabetes_X_train)
diabetes_X_train1=np.array(diabetes_X_train1)
diabetes_X_train=diabetes_X_train.reshape(-1, 1)
diabetes_X_train1=diabetes_X_train1.reshape(-1, 1)

# Create linear regression object
regr = linear_model.LinearRegression()


# Train the model using the training sets
regr.fit(diabetes_X_train1, diabetes_y_train1)

# Make predictions using the testing set
diabetes_y_pred = regr.predict(diabetes_X_test)

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean squared error


# Plot outputs
#plt.plot(np.array([1,2,3,4,5,6,7]), diabetes_y_train1, color='blue', linewidth=3)
#plt.plot(np.array([1,2,3,4,5,6,7]), diabetes_X_train1, color='red', linewidth=3)



#plt.show()