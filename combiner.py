import pandas as pd
import numpy as np
import datetime
from dateutil import parser
import time
import os
import cvxopt as cv
import cvxopt.solvers

windowHalf=30

directory_to_check = "/home/affectech/Downloads/PC/Project"# Which directory do you want to start with?


def my_function(directory):
      print("Listing: " + directory)

      try:
          EDA=pd.read_csv(directory+'/label_avg_combinationEDA.csv')
          HR=pd.read_csv(directory+'/affectech_istanbul_120sec.csv',skiprows=0)
      except:
          print("File not found exception")
          return


      EDAMatrix=EDA.ix[:,:]
      HRMatrix=HR.ix[:,:]
      
      HrLen=len(HRMatrix)
      EDALen=len(EDAMatrix)
      
      lastStr=""
      
      for i in range(0,EDALen):
          startTimestamp=EDAMatrix.values[i][0]
          endTimestamp = EDAMatrix.values[i][1]
         
          for j in range(0,HrLen):
              startTimestampHR=HRMatrix.values[j][22]
              endTimestampHR = HRMatrix.values[j][23]
              
              if(startTimestampHR == startTimestamp and endTimestamp==endTimestampHR ):
                  segStr=str(EDAMatrix.values[i][2])+","+str(EDAMatrix.values[i][3])+","+str(EDAMatrix.values[i][4])+","+str(EDAMatrix.values[i][5])+","+str(EDAMatrix.values[i][6])+","+str(EDAMatrix.values[i][7])+","+str(EDAMatrix.values[i][8])+","+str(EDAMatrix.values[i][9])+","+str(EDAMatrix.values[i][10])+","+str(EDAMatrix.values[i][11])+","+str(EDAMatrix.values[i][0]) +","+str(EDAMatrix.values[i][1])+","+str(HRMatrix.values[j][0])+","+str(HRMatrix.values[j][1])+","+str(HRMatrix.values[j][2])+","+str(HRMatrix.values[j][3])+","+str(HRMatrix.values[j][4])+","+str(HRMatrix.values[j][5])+","+str(HRMatrix.values[j][6])+","+str(HRMatrix.values[j][7])+","+str(HRMatrix.values[j][8])+","+str(HRMatrix.values[j][9])+","+str(HRMatrix.values[j][10])+","+str(HRMatrix.values[j][11])+","+str(HRMatrix.values[j][12])+","+str(HRMatrix.values[j][13])+","+str(HRMatrix.values[j][14])+","+str(HRMatrix.values[j][15])+","+str(HRMatrix.values[j][16])+","+str(HRMatrix.values[j][17])+","+str(HRMatrix.values[j][18])+","+str(HRMatrix.values[j][19])+","+str(HRMatrix.values[j][20])+"\n"
                  lastStr=lastStr+segStr
              


      return lastStr



# Get all the subdirectories of directory_to_check recursively and store them in a list:
directories = [os.path.abspath(x[0]) for x in os.walk(directory_to_check)]
#directories.remove(os.path.abspath(directory_to_check)) # If you don't want your main directory included

string=""
f = open('/home/affectech/Desktop/combined.csv','w')
f.write("mean"+","+"std"+","+"peak"+","+"strong_peak"+","+"perc20"+","+"perc80"+","+"quartDev"+","+"dataQual"+","+"label_sub"+","+"session_label"+","+"session"+","+"device"+","+"fv_e1"+","+"fv_e2"+","+"fv_e3"+","+"fv_e4"+","+"fv_e5"+","+"fv_e6"+","+"fv_e7"+","+"fv_e8"+","+"fv_e9"+","+"fv_e10"+","+"fv_e11"+","+"fv_e12"+","+"fv_e13"+","+"fv_e14"+","+"fv_e15"+","+"fv_e16"+","+"fv_e17"+","+"fv_e18"+","+"fv_e19"+","+"fv_e20"+","+"fv_e21"+"\n")
for i in directories:
      os.chdir(i)         # Change working Directory
      #if(os.path.isfile(i+'/EDA.csv')==False):
      #    continue
      lineStr=my_function(i)
      if not (lineStr is None):
          f.write(lineStr)

f.close()