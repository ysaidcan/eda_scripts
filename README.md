eda-explorer
============

Scripts to detect artifacts and in electrodermal activity (EDA) data. Note that these scripts are written in Python 2.7.


Version 0.4

Please also cite this project:
Taylor, S., Jaques, N., Chen, W., Fedor, S., Sano, A., & Picard, R. Automatic identification of artifacts in electrodermal activity data. In Engineering in Medicine and Biology Conference. 2015.


Download Anaconda2.7 . For ubuntu i worked with Anaconda2-5.3.1-Linux-x86_64.sh

I recommend PyCharm

In pycharm, select conda as interpreter.


Required python packages: 
===
- numpy: 1.9.2 
- scipy > 0.16 -> latest 1.5.2
- pandas: 0.16.0
- scikit-learn: 0.16.1
- PyWavelets: 0.2.2 -> work with 0.5.2 also.
- cvxopt

First run artifact detection from the command line:
==
python EDA-Artifact-Detection-Script.py


Second run peak detection:
==
python EDA-Peak-Detection-Script.py

Descriptions of the algorithm settings can be found at http://eda-explorer.media.mit.edu/info/

Third run artifact_reader.py

Fourth run kmeansCluster.py

We are using cvxEDA tool to discriminate Phasic and Tonic components of EDA Signal.
______________________________________________________________________________

============================= c v x E D A ====================================
______________________________________________________________________________

This program implements the cvxEDA algorithm for the analysis of electrodermal
activity (EDA) using methods of convex optimization, described in:

A Greco, G Valenza, A Lanata, EP Scilingo, and L Citi
"cvxEDA: a Convex Optimization Approach to Electrodermal Activity Processing"
IEEE Transactions on Biomedical Engineering, 2015
DOI: 10.1109/TBME.2015.2474131

It is based on a model which describes EDA as the sum of three terms: the
phasic component, the tonic component, and an additive white Gaussian noise
term incorporating model prediction errors as well as measurement errors and
artifacts.
This model is physiologically inspired and fully explains EDA through a
rigorous methodology based on Bayesian statistics, mathematical convex
optimization and sparsity.

The algorithm was evaluated in three different experimental sessions
(see paper) to test its robustness to noise, its ability to separate and
identify stimulus inputs, and its capability of properly describing the
activity of the autonomic nervous system in response to strong affective
stimulation.
______________________________________________________________________________

Copyright (C) 2014-2015 Luca Citi, Alberto Greco

Notes:
===

1. Currently, these files are written with the assumption that the sample rate is an integer power of 2. 

2. Keep the "classify.py" and "SVMBinary.p" and "SVMMulticlass.p" files in the same directory.

3. Please visit eda.explorer.media.mit.edu to use the web-based version
