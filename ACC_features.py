
import pandas as pd
import numpy as np
import datetime
from dateutil import parser
import time
import os



directory_to_check = "/home/affectech/Desktop/Istanbul_Training" # Which directory do you want to start with?


def my_function(directory):
    print("Listing: " + directory)
    # print("\t-" + "\n\t-".join(os.listdir("."))) # List current working directory

    # reading EDA data, artifact and peak data
    try:
        accfeatures = pd.read_csv(directory + '/accfeatures_01.csv')
        sessionInfo = pd.read_csv(directory + '/labels.csv')
    except:
        print("File not found exception")
        return

    # parsing files into python arrays
    stepCount=accfeatures.ix[0,2]
    stillness = accfeatures.ix[0,3]
    session = sessionInfo.ix[0,0]
    label = sessionInfo.ix[0,1]


    # print("Mean and Standard Deviation are = " + "%.4f" % mean + "---" +"%.4f" % std)
    oneStr = str(stepCount) + "," + str(stillness) + "," + str(session) + "," + str(label)  + "\n"
    return oneStr


# Get all the subdirectories of directory_to_check recursively and store them in a list:
directories = [os.path.abspath(x[0]) for x in os.walk(directory_to_check)]
directories.remove(os.path.abspath(directory_to_check))  # If you don't want your main directory included

string = ""
f = open('accProps.csv', 'w')
f.write(
    "stepCount" + "," + "stillness" + "," + "session" + "," + "label" + "\n")
for i in directories:
    os.chdir(i)  # Change working Directory
    if (os.path.isfile(i + '/ACC.csv') == False):
        continue
    lineStr = my_function(i)
    if not (lineStr is None):
        f.write(lineStr)

f.close()












