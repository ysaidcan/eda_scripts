#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 05:00:48 2018

@author: root
"""

import os

directory_to_check = "/home/affectech/Downloads/eda-explorer-master" # Which directory do you want to start with?

def my_function(directory):
      print("Listing: " + directory)
      print("\t-" + "\n\t-".join(os.listdir("."))) # List current working directory

# Get all the subdirectories of directory_to_check recursively and store them in a list:
directories = [os.path.abspath(x[0]) for x in os.walk(directory_to_check)]
directories.remove(os.path.abspath(directory_to_check)) # If you don't want your main directory included

for i in directories:
      os.chdir(i)         # Change working Directory
      my_function(i) 