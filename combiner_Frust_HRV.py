import pandas as pd
import numpy as np
import datetime
from dateutil import parser
import time
import os
import cvxopt as cv
import cvxopt.solvers

windowHalf=30

directory_to_check = "/home/affectech/Desktop"# Which directory do you want to start with?


def my_function(directory):
      print("Listing: " + directory)

      try:
          EDA=pd.read_csv(directory+'/HRV_with_weather.csv')
          Frust=pd.read_csv(directory+'/Affectech_Istanbul_Frust.csv',skiprows=0)
      except:
          print("File not found exception")
          return


      EDAMatrix=EDA.ix[:,:]
      HRMatrix=Frust.ix[:,:]
      
      HrLen=len(HRMatrix)
      EDALen=len(EDAMatrix)
      
      lastStr=""
      
      for i in range(0,EDALen):
          startTimestamp=EDAMatrix.values[i][22]
          endTimestamp = EDAMatrix.values[i][23]
         
          for j in range(0,HrLen):
              startTimestampHR=HRMatrix.values[j][2]
              endTimestampHR = HRMatrix.values[j][0]
              
              if(startTimestampHR == startTimestamp and endTimestamp==endTimestampHR ):
                  segStr=str(EDAMatrix.values[i][0])+","+str(EDAMatrix.values[i][1])+","+str(EDAMatrix.values[i][2])+","+str(EDAMatrix.values[i][3])+","+str(EDAMatrix.values[i][4])+","+str(EDAMatrix.values[i][5])+","+str(EDAMatrix.values[i][6])+","+str(EDAMatrix.values[i][7])+","+str(EDAMatrix.values[i][8])+","+str(EDAMatrix.values[i][9])+","+str(EDAMatrix.values[i][10]) +","+str(EDAMatrix.values[i][11])+","+str(EDAMatrix.values[i][12])+","+str(EDAMatrix.values[i][13])+","+str(EDAMatrix.values[i][14])+","+str(EDAMatrix.values[i][15])+","+str(EDAMatrix.values[i][16])+","+str(EDAMatrix.values[i][17])+","+str(EDAMatrix.values[i][18])+","+str(EDAMatrix.values[i][19])+","+str(EDAMatrix.values[i][20])+","+str(EDAMatrix.values[i][21])+","+str(EDAMatrix.values[i][22])+","+str(EDAMatrix.values[i][23])+","+str(EDAMatrix.values[i][24])+","+str(EDAMatrix.values[i][25])+","+str(EDAMatrix.values[i][26])+","+str(EDAMatrix.values[i][27])+","+str(EDAMatrix.values[i][28])+","+str(EDAMatrix.values[i][29])+","+str(EDAMatrix.values[i][30])+","+str(EDAMatrix.values[i][31])+","+str(EDAMatrix.values[i][32])+","+str(HRMatrix.values[j][3])+","+str(HRMatrix.values[j][4])+"\n"
                  lastStr=lastStr+segStr
              


      return lastStr



# Get all the subdirectories of directory_to_check recursively and store them in a list:
directories = [os.path.abspath(x[0]) for x in os.walk(directory_to_check)]
#directories.remove(os.path.abspath(directory_to_check)) # If you don't want your main directory included

string=""
f = open('/home/affectech/Desktop/combined_HRV.csv','w')
f.write("f1"+","+"f2"+","+"f3"+","+"f4"+","+"f5"+","+"f6"+","+"f7"+","+"f8"+","+"f9"+","+"f10"+","+"f11"+","+"f12"+","+"fv_e14"+","+"fv_e15"+","+"fv_e16"+","+"fv_e17"+","+"fv_e18"+","+"fv_e19"+","+"fv_e20"+","+"fv_e21"+","+"fv_e22"+","+"class"+","+"session"+","+"device_id"+","+"Temp"+","+"P0"+","+"P"+","+"P_diff"+","+"Hum"+","+"Wind"+","+"Cloud"+","+"Temp_dew"+","+"Precipitation"+","+"frust_2"+","+"frust_3"+"\n")
for i in directories:
      os.chdir(i)         # Change working Directory
      #if(os.path.isfile(i+'/EDA.csv')==False):
      #    continue
      lineStr=my_function(i)
      if not (lineStr is None):
          f.write(lineStr)

f.close()