#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 14:04:40 2018

@author: root
"""
k=2
accuracy_results=0
if(k==2):
    for i in range(0, len(y_kmeans)):
        if(y_kmeans[i]==1 and Subjective_Labels[i]==2):
            accuracy_results=accuracy_results+1
        elif(y_kmeans[i]==0 and Subjective_Labels[i]==1):
            accuracy_results=accuracy_results+1
        elif(y_kmeans[i]==0 and Subjective_Labels[i]==0):
            accuracy_results=accuracy_results+1
elif(k==3):   
    for i in range(0, len(y_kmeans)):
        if(y_kmeans[i]==2 and Subjective_Labels[i]==2):
            accuracy_results=accuracy_results+1
        elif(y_kmeans[i]==1 and Subjective_Labels[i]==1):
            accuracy_results=accuracy_results+1
        elif(y_kmeans[i]==0 and Subjective_Labels[i]==0):
            accuracy_results=accuracy_results+1  
            
accuracy_results=float(accuracy_results)/len(y_kmeans)
print(str(accuracy_results))        
        
        