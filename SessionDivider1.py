#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 22:00:27 2018

@author: root
"""
import pandas as pd
import numpy as np


try:
    features = pd.read_csv('./label_avg_copy.csv')
    eventLogs = pd.read_csv('./HRV_120sec.csv' )
except:
    print("File not found exception")


    # parsing files into python arrays

SessionID = features.ix[:, 0]
Device = features.ix[:, 1]
mean = features.ix[:, 2]
peak = features.ix[:, 3]
perc20 = features.ix[:, 4]
perc80 = features.ix[:, 5]
quartDev = features.ix[:, 6]
std = features.ix[:, 7]
strong_peak = features.ix[:, 8]
dataQualityRatio = features.ix[:, 9]
sessionLabel = features.ix[:, 11]



fv1 = eventLogs.ix[:, 0]
fv2 = eventLogs.ix[:, 1]
fv3 = eventLogs.ix[:, 2]
fv4 = eventLogs.ix[:, 3]
fv5 = eventLogs.ix[:, 4]
fv6 = eventLogs.ix[:, 5]
fv7 = eventLogs.ix[:, 6]
fv8 = eventLogs.ix[:, 7]
fv9 = eventLogs.ix[:, 8]
fv10 = eventLogs.ix[:, 9]
fv11 = eventLogs.ix[:, 10]
fv12 = eventLogs.ix[:, 11]
fv13 = eventLogs.ix[:, 12]
fv14 = eventLogs.ix[:, 13]
fv15 = eventLogs.ix[:, 14]
fv16 = eventLogs.ix[:, 15]
fv17 = eventLogs.ix[:, 16]
HRquality = eventLogs.ix[:, 20]
class2 = eventLogs.ix[:, 21]
session2 = eventLogs.ix[:, 22]
deviceID2 = eventLogs.ix[:, 23]


sessionMat=np.zeros((len(std),29),dtype=float)
sessionMat.fill(np.nan)
session_string= np.empty(len(std),dtype="S10")
session_string.fill(np.nan)

device_id_string= np.empty(len(std),dtype="S10")
device_id_string.fill(np.nan)
for i in range(0, len(std)):
    EDAsession=SessionID[i]
    EDAdevice=Device[i]
    for j in range(0,len(eventLogs)):
        HRVsession=session2[j]
        HRVdevice=deviceID2[j]
        if(HRVsession==EDAsession  and HRVdevice==EDAdevice):
            sessionMat[i,0]=mean[i]
            sessionMat[i, 1] = std[i]
            sessionMat[i, 2] = peak[i]
            sessionMat[i,3] = strong_peak[i]
            sessionMat[i,4] = perc20[i]
            sessionMat[i,5] = perc80[i]
            sessionMat[i,6] = quartDev[i]
            sessionMat[i,7] = dataQualityRatio[i]
            sessionMat[i,8]=fv1[j]
            sessionMat[i, 9] = fv2[j]
            sessionMat[i, 10] = fv3[j]
            sessionMat[i, 11] = fv4[j]
            sessionMat[i, 12] = fv5[j]
            sessionMat[i, 13] = fv6[j]
            sessionMat[i, 14] = fv7[j]
            sessionMat[i, 15] = fv8[j]
            sessionMat[i, 16] = fv9[j]
            sessionMat[i, 17] = fv10[j]
            sessionMat[i, 18] = fv11[j]
            sessionMat[i, 19] = fv12[j]
            sessionMat[i,20]=fv13[j]
            sessionMat[i, 21] = fv14[j]
            sessionMat[i, 22] = fv15[j]
            sessionMat[i, 23] = fv16[j]
            sessionMat[i, 24] = fv17[j]

            sessionMat[i,25]=HRquality[j]
            sessionMat[i,26]=class2[j]
            session_string[i]=session2[j]
            device_id_string[i]=deviceID2[j]

sessID=pd.Series(session_string)
deviID=pd.Series(device_id_string)
dataset = pd.DataFrame({'Mean':sessionMat[:,0],'Std':sessionMat[:,1],'Peak':sessionMat[:,2],'Strong_Peak':sessionMat[:,3],'Perc20':sessionMat[:,4],
'Perc80':sessionMat[:,5],'QuartDev':sessionMat[:,6],'DataQual':sessionMat[:,7],'fv1':sessionMat[:,8],'fv2':sessionMat[:,9],
'fv3':sessionMat[:,10],'fv4':sessionMat[:,11],'fv5':sessionMat[:,12],'fv6':sessionMat[:,13],'fv7':sessionMat[:,14],'fv8':sessionMat[:,15],'fv9':sessionMat[:,16]
                        ,'fv10':sessionMat[:,17],'fv11':sessionMat[:,18],'fv12':sessionMat[:,19],'fv13':sessionMat[:,20],'fv14':sessionMat[:,21],'fv15':sessionMat[:,22],'fv16':sessionMat[:,23],'fv17':sessionMat[:,24],'HRQuality':sessionMat[:,25] ,'context':sessionMat[:,26],'SessID':sessID,'devID':deviID})

dataset.dropna(axis=0, how='any', inplace=True)

dataset.to_csv('./combined.csv', encoding='utf-8')












