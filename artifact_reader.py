#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 22:00:27 2018

@author: root
"""
import pandas as pd
import numpy as np
import datetime
from dateutil import parser
import time
import os
import cvxopt as cv
import cvxopt.solvers

segmentLength= 120 #in seconds
windowLength= segmentLength / 2 #in seconds
sample_interval=0.25

directory_to_check = "/home/affectech/Desktop/WESAD" # Which directory do you want to start with?

def cvxEDA(y, delta, tau0=2., tau1=0.7, delta_knot=10., alpha=8e-4, gamma=1e-2,
           solver=None, options={'reltol':1e-9}):
    """CVXEDA Convex optimization approach to electrodermal activity processing

    This function implements the cvxEDA algorithm described in "cvxEDA: a
    Convex Optimization Approach to Electrodermal Activity Processing"
    (http://dx.doi.org/10.1109/TBME.2015.2474131, also available from the
    authors' homepages).

    Arguments:
       y: observed EDA signal (we recommend normalizing it: y = zscore(y))
       delta: sampling interval (in seconds) of y
       tau0: slow time constant of the Bateman function
       tau1: fast time constant of the Bateman function
       delta_knot: time between knots of the tonic spline function
       alpha: penalization for the sparse SMNA driver
       gamma: penalization for the tonic spline coefficients
       solver: sparse QP solver to be used, see cvxopt.solvers.qp
       options: solver options, see:
                http://cvxopt.org/userguide/coneprog.html#algorithm-parameters

    Returns (see paper for details):
       r: phasic component
       p: sparse SMNA driver of phasic component
       t: tonic component
       l: coefficients of tonic spline
       d: offset and slope of the linear drift term
       e: model residuals
       obj: value of objective function being minimized (eq 15 of paper)
    """

    n = len(y)
    y = cv.matrix(y)

    # bateman ARMA model
    a1 = 1./min(tau1, tau0) # a1 > a0
    a0 = 1./max(tau1, tau0)
    ar = np.array([(a1*delta + 2.) * (a0*delta + 2.), 2.*a1*a0*delta**2 - 8.,
        (a1*delta - 2.) * (a0*delta - 2.)]) / ((a1 - a0) * delta**2)
    ma = np.array([1., 2., 1.])

    # matrices for ARMA model
    i = np.arange(2, n)
    A = cv.spmatrix(np.tile(ar, (n-2,1)), np.c_[i,i,i], np.c_[i,i-1,i-2], (n,n))
    M = cv.spmatrix(np.tile(ma, (n-2,1)), np.c_[i,i,i], np.c_[i,i-1,i-2], (n,n))

    # spline
    delta_knot_s = int(round(delta_knot / delta))
    spl = np.r_[np.arange(1.,delta_knot_s), np.arange(delta_knot_s, 0., -1.)] # order 1
    spl = np.convolve(spl, spl, 'full')
    spl /= max(spl)
    # matrix of spline regressors
    i = np.c_[np.arange(-(len(spl)//2), (len(spl)+1)//2)] + np.r_[np.arange(0, n, delta_knot_s)]
    nB = i.shape[1]
    j = np.tile(np.arange(nB), (len(spl),1))
    p = np.tile(spl, (nB,1)).T
    valid = (i >= 0) & (i < n)
    B = cv.spmatrix(p[valid], i[valid], j[valid])

    # trend
    C = cv.matrix(np.c_[np.ones(n), np.arange(1., n+1.)/n])
    nC = C.size[1]

    # Solve the problem:
    # .5*(M*q + B*l + C*d - y)^2 + alpha*sum(A,1)*p + .5*gamma*l'*l
    # s.t. A*q >= 0

    old_options = cv.solvers.options.copy()
    cv.solvers.options.clear()
    cv.solvers.options.update(options)
    if solver == 'conelp':
        # Use conelp
        z = lambda m,n: cv.spmatrix([],[],[],(m,n))
        G = cv.sparse([[-A,z(2,n),M,z(nB+2,n)],[z(n+2,nC),C,z(nB+2,nC)],
                    [z(n,1),-1,1,z(n+nB+2,1)],[z(2*n+2,1),-1,1,z(nB,1)],
                    [z(n+2,nB),B,z(2,nB),cv.spmatrix(1.0, range(nB), range(nB))]])
        h = cv.matrix([z(n,1),.5,.5,y,.5,.5,z(nB,1)])
        c = cv.matrix([(cv.matrix(alpha, (1,n)) * A).T,z(nC,1),1,gamma,z(nB,1)])
        res = cv.solvers.conelp(c, G, h, dims={'l':n,'q':[n+2,nB+2],'s':[]})
        obj = res['primal objective']
    else:
        # Use qp
        Mt, Ct, Bt = M.T, C.T, B.T
        H = cv.sparse([[Mt*M, Ct*M, Bt*M], [Mt*C, Ct*C, Bt*C], 
                    [Mt*B, Ct*B, Bt*B+gamma*cv.spmatrix(1.0, range(nB), range(nB))]])
        f = cv.matrix([(cv.matrix(alpha, (1,n)) * A).T - Mt*y,  -(Ct*y), -(Bt*y)])
        res = cv.solvers.qp(H, f, cv.spmatrix(-A.V, A.I, A.J, (n,len(f))),
                            cv.matrix(0., (n,1)), solver=solver)
        obj = res['primal objective'] + .5 * (y.T * y)
    cv.solvers.options.clear()
    cv.solvers.options.update(old_options)

    l = res['x'][-nB:]
    d = res['x'][n:n+nC]
    t = B*l + C*d
    q = res['x'][:n]
    p = A * q
    r = M * q
    e = y - r - t

    return (np.array(a).ravel() for a in (r, p, t, l, d, e, obj))


def my_function(directory):
      print("Listing: " + directory)
      #print("\t-" + "\n\t-".join(os.listdir("."))) # List current working directory
      
      #reading EDA data, artifact and peak data
      try:
          start=pd.read_csv(directory+'/EDA.csv')
          EDA=pd.read_csv(directory+'/EDA.csv',skiprows=1)
          df = pd.read_csv(directory+'/artifacts_01.csv')
          peaks = pd.read_csv(directory+'/peaks_01.csv')
          session_label = pd.read_csv(directory+'/labels.csv')
          IBI=pd.read_csv(directory+'/IBI.csv',skiprows=0)
      except:
          print("File not found exception")
          return
        
      #parsing files into python arrays  
      dates=df.ix[:,0]
      label=df.ix[:,2]
      data=EDA.ix[:,0]
      IBI_start=IBI.ix[0,0]
      sessionStart=0
      datesFloat = np.zeros(len(dates))

      for i in range(0, len(label)):
          timeA = parser.parse(dates[i])
          datesFloat[i]=time.mktime(timeA.timetuple())
        
      EDA_data_arr=np.zeros(len(data))
      EDA_datetime=np.zeros(len(data))
      for i in range (0,len(label)-1):
          for j in range (0,20):
              EDA_data_arr[i*20+j]=label[i]
        
      datetime_object = datetime.datetime.strptime(dates[0], '%Y-%m-%d %H:%M:%S')
      datetime_object_last = datetime.datetime.strptime(dates[len(dates)-1], '%Y-%m-%d %H:%M:%S')
      dt = parser.parse(dates[0])
      dt_float=time.mktime(dt.timetuple())
      
      #calculate start time as a float from 1970
      startTime=float(start.columns[0])
      
      #calculate the number of segments inside the data
      #data 4000 sample segment 100secs Sample 4Hz 10 segments
      #if(not directory.endswith('1')):
      #    IBI_start=0

      #if (directory.endswith('2')):
       #   sessionStart = 2400
      #if (directory.endswith('3')):
       #   sessionStart = 2400
      #if (directory.endswith('4')):
       #   sessionStart = 3900
      #if (directory.endswith('5')):
       #   sessionStart = 3600
      #if (directory.endswith('6')):
      #    sessionStart = 5100
      #if (directory.endswith('7')):
       #   sessionStart = 4500

          
      numOfSegments=(len(data)-int(IBI_start/sample_interval))/(segmentLength/sample_interval)
      numOfSegments=int(numOfSegments)
      
      
          
      #find peak times in float since 1970 linux time  
      for i in range (0,len(data)):
          EDA_datetime[i]=time.mktime(dt.timetuple())+i*0.25
            
        
      EDA_data_arr=pd.Series(EDA_data_arr)
      EDA_datetime=pd.Series(EDA_datetime)
      EDA_data=pd.concat([EDA_datetime, data, EDA_data_arr], axis=1)
        
      peakstart=peaks.ix[:,0]
      peakstart_index=np.zeros(len(peakstart))
      peakstart_time=np.zeros(len(peakstart))
        
        
        
      for i in range(0,len(peakstart)):
          if(len(peakstart[i])>20):
              peaktime=datetime.datetime.strptime(peakstart[i], '%Y-%m-%d %H:%M:%S.%f')
              microsecond=peaktime.microsecond/1e6
              if(microsecond==0.875):
                  microsecond=0.75
              elif(microsecond==0.125):
                  microsecond=0
              elif(microsecond==0.375):
                  microsecond=0.25
              elif(microsecond==0.625):
                  microsecond=0.5
              peakstart_index[i] = (time.mktime(peaktime.timetuple()) + microsecond -time.mktime(dt.timetuple()))*4
              peakstart_time[i]=time.mktime(peaktime.timetuple()) + microsecond
          else:
               peaktime=datetime.datetime.strptime(peakstart[i], '%Y-%m-%d %H:%M:%S')
               peakstart_index[i] = time.mktime(peaktime.timetuple()) -time.mktime(dt.timetuple())
               peakstart_time[i]=time.mktime(peaktime.timetuple())
        
      isPeakArtifact= np.ones(len(peakstart), dtype=bool)     
        
      
      #determine if the peak is artifact using artifact detection tool from MIT 
      #Taylor
      counter=0
      for i in peakstart_index:
          if(EDA_data_arr[i]==-1):
              isPeakArtifact[counter]=True
          else:
              isPeakArtifact[counter]=False
          counter=counter+1
            
      peakstart_index=pd.Series(peakstart_index)
      peakstart_time=pd.Series(peakstart_time)
      strongPeak= np.ones(len(peakstart), dtype=float)
      weakPeak=np.ones(len(peakstart), dtype=float)
      
      isPeakArtifact=pd.Series(isPeakArtifact)
      peak_prop=peaks.iloc[:,1:8]
      peak_data=pd.concat([peakstart_index, peakstart_time, peak_prop, isPeakArtifact], axis=1)
      amplitude = peak_data['amp']
      amplitude=np.array(amplitude)
      if(len(peakstart)==0):
          print("")
          truePeak=0
      else:        
          faulty=sum(float(num) == -1 for num in label)
          healthy=float(len(label)-faulty)/len(label)
          print("Healthy Data ratio is = "+"%.4f" % healthy) 
          fakePeak=float(sum(num == True for num in isPeakArtifact))/len(peakstart)
          filteredPeaks=isPeakArtifact[(amplitude>1)]
          if(len(filteredPeaks)==0):
              weakPeak=0
              strongPeak=0
          else:
              weakPeak=float(sum(num == True for num in filteredPeaks))/len(filteredPeaks)
              strongPeak=1-weakPeak
          
          truePeak=1-fakePeak
          truePeak=truePeak*len(isPeakArtifact)
          strongPeak=strongPeak*len(filteredPeaks)
        #datetime.fromtimestamp ile timestampten datetime'a geçebilirsin
      start=time.mktime(datetime_object.timetuple()) 
      end=time.mktime(datetime_object_last.timetuple()) 
      
      
      (r, p, t, l, d, e, obj)=cvxEDA(data, 0.25, tau0=2., tau1=0.7, delta_knot=10., alpha=8e-4, gamma=1e-2,
           solver=None, options={'reltol':1e-9})
        
      mean=np.mean(t)
      max=np.max(t)
      min=np.min(t)
      std=np.std(t)
      perc20=np.percentile(t,20)
      perc80=np.percentile(t,80)
      QuartDev=np.percentile(t,75)-np.percentile(t,25)
      
      #----------------------------------------
      
      meanCount=np.zeros(numOfSegments*2-1)
      maxCount=np.zeros(numOfSegments*2-1)
      minCount=np.zeros(numOfSegments*2-1)
      stdCount=np.zeros(numOfSegments*2-1)
      peakCount=np.zeros(numOfSegments*2-1)
      strongPeakCount=np.zeros(numOfSegments*2-1)
      perc20Count=np.zeros(numOfSegments*2-1)
      perc80Count=np.zeros(numOfSegments*2-1)
      QuartDevCount=np.zeros(numOfSegments*2-1)
      startTimestamp=np.zeros(numOfSegments*2-1)
      endTimestamp=np.zeros(numOfSegments*2-1)
      dataQualitySegment=np.zeros(numOfSegments*2-1)
      derivative=np.zeros(numOfSegments*2-1)
      
      DoubleSegments=numOfSegments*2-1
      
      #define mean, std and  peakCount arrays for each segment
      
      i_count=0
      for i in range (0, numOfSegments):      
          for j in range (0, len(peakstart_time)):
              if(0 < peakstart_time[j]-start-(i*segmentLength)- IBI_start < segmentLength and isPeakArtifact[j]==False):
                  #if peak is in range of the segment and is not artifact
                  
                  peakCount[i_count]=peakCount[i_count]+1
                  
                  if(amplitude[j]>1):
                      strongPeakCount[i_count]=strongPeakCount[i_count]+1

          for j in range(0, len(label)):
                  if (0 < datesFloat[j] - start - (i * segmentLength) - IBI_start < segmentLength and
                          label[j] == -1):
                      # if peak is in range of the segment and is not artifact

                      dataQualitySegment[i_count] = dataQualitySegment[i_count] + 1

          peakCount[i_count]=peakCount[i_count]*100.0/segmentLength
          strongPeakCount[i_count]=strongPeakCount[i_count]*100.0/segmentLength        
          dataQualitySegment[i_count]=dataQualitySegment[i_count]/(segmentLength/5);
          i_count=i_count+1
          if(i_count < DoubleSegments):
              for j in range (0, len(peakstart_time)):            
                  if(0 + windowLength < peakstart_time[j]-start-(i*segmentLength) - IBI_start < windowLength + segmentLength and isPeakArtifact[j]==False):
                  #if peak is in range of the segment and is not artifact
                  
                      peakCount[i_count]=peakCount[i_count]+1
                  
                      if(amplitude[j]>1):
                          strongPeakCount[i_count]=strongPeakCount[i_count]+1

          if (i_count < DoubleSegments):
              for j in range(0, len(peakstart_time)):
                  if (0 + windowLength < datesFloat[j] - start - (i * segmentLength) - IBI_start < windowLength + segmentLength and  label[j] == -1):
                              # if peak is in range of the segment and is not artifact

                      dataQualitySegment[i_count] = dataQualitySegment[i_count] + 1
                  
                  # add to peak count
              peakCount[i_count]=peakCount[i_count]*100.0/segmentLength  
              strongPeakCount[i_count]=strongPeakCount[i_count]*100.0/segmentLength
              dataQualitySegment[i_count] = dataQualitySegment[i_count] / (segmentLength / 5);
              i_count=i_count+1  
          

      time_length=end-start
      if(time_length==0):
          truePeak=0;
          strongPeak=0;
          artifactRatio=0
      else:
          
      
      # the total time
          truePeak=truePeak*100/time_length
          strongPeak=strongPeak*100/time_length
          artifactRatio = sum(dataQualitySegment) / (time_length /5)
      print("True Peak ratio is = "+"%.4f" % truePeak)  
      
     
      count=0;  
      
      for i in range (0, numOfSegments):
          
          before=(i)*segmentLength/sample_interval+int(IBI_start/sample_interval) #start index of the segment
          after=(i+1)*segmentLength/sample_interval+int(IBI_start/sample_interval) #end index of the segment
          if(after<= len(data)):
              dataSeg=data[int(before):int(after)]
              (r, p, t, l, d, e, obj)=cvxEDA(dataSeg, 0.25, tau0=2., tau1=0.7, delta_knot=10., alpha=8e-4, gamma=1e-2,
               solver=None, options={'reltol':1e-9})
              meanCount[count]=np.mean(t)
              derivative[count]= (t[-1]-t[0])/windowLength
              stdCount[count]=np.std(t)
              maxCount[count]=np.max(t)
              minCount[count]=np.min(t)
              perc20Count[count]=np.percentile(t,20)
              perc80Count[count]=np.percentile(t,80)
              QuartDevCount[count]=np.percentile(t,75)-np.percentile(t,25)
              startTimestamp[count]=before*sample_interval+startTime+sessionStart
              endTimestamp[count]=after*sample_interval+startTime+sessionStart    
              count=count+1
              
              before=(i)*segmentLength/sample_interval+int(windowLength/sample_interval)+int(IBI_start/sample_interval) #start index of the segment
              after=(i+1)*segmentLength/sample_interval+int(windowLength/sample_interval)+int(IBI_start/sample_interval) #end index of the segment
              if(after<= len(data) and not count==DoubleSegments):
                  
                  dataSeg=data[int(before):int(after)]
                  (r, p, t, l, d, e, obj)=cvxEDA(dataSeg, 0.25, tau0=2., tau1=0.7, delta_knot=10., alpha=8e-4, gamma=1e-2,
                  solver=None, options={'reltol':1e-9})
                  meanCount[count]=np.mean(t)
                  derivative[count] = (t[-1] - t[0]) / windowLength
                  stdCount[count]=np.std(t)
                  maxCount[count]=np.max(t)
                  minCount[count]=np.min(t)
                  perc20Count[count]=np.percentile(t,20)
                  perc80Count[count]=np.percentile(t,80)
                  QuartDevCount[count]=np.percentile(t,75)-np.percentile(t,25)
                  startTimestamp[count]=before*sample_interval+startTime+sessionStart
                  endTimestamp[count]=after*sample_interval+startTime+sessionStart 
                  count=count+1
          
   
          
          
      
      
      #these are the subjective labels and session ids
      label_subjective=session_label['Label'].iloc[0]
      Session=session_label['Session'].iloc[0]
      lastStr=""
      for i in range (0, 2*numOfSegments-1):
          segStr=str(meanCount[i])+","+str(stdCount[i])+","+str(peakCount[i])+","+str(strongPeakCount[i])+","+str(perc20Count[i])+","+str(perc80Count[i])+","+str(QuartDevCount[i])+","+Session+","+str(label_subjective)+","+str(startTimestamp[i])+","+str(endTimestamp[i])+","+str(maxCount[i])+","+str(minCount[i])+","+str(dataQualitySegment[i])+","+str(derivative[i])+"\n"
          lastStr=lastStr+segStr
      
      #lastStr=str(mean)+","+str(std)+","+str(truePeak)+","+Session+","+str(label_subjective)+"\n"
        
      #print("Mean and Standard Deviation are = " + "%.4f" % mean + "---" +"%.4f" % std)
      oneStr=str(mean)+","+str(std)+","+str(truePeak)+","+str(strongPeak)+","+str(perc20)+","+str(perc80)+","+str(QuartDev)+","+Session+"-"+str(i+1)+","+str(label_subjective)+","+str(startTimestamp)+","+str(endTimestamp)+","+str(max)+","+str(min)+","+str(artifactRatio)+"\n"
      return lastStr  

# Get all the subdirectories of directory_to_check recursively and store them in a list:
directories = [os.path.abspath(x[0]) for x in os.walk(directory_to_check)]
directories.remove(os.path.abspath(directory_to_check)) # If you don't want your main directory included

string=""
f = open('peakProps_120.csv','w')
f.write("mean"+","+"std"+","+"peak"+","+"strong_peak"+","+"perc20"+","+"perc80"+","+"quartDev"+","+"session"+","+"label_subjective"+","+"startTimestamp"+","+"endTimeStamp"+","+"max"+","+"min"+","+"dataQualityRatio"+","+"derivative"+"\n")
for i in directories:
      os.chdir(i)         # Change working Directory
      if(os.path.isfile(i+'/EDA.csv')==False):
          continue
      lineStr=my_function(i)
      if not (lineStr is None):
          f.write(lineStr)

f.close()



      



    




