import pandas as pd
import numpy as np
import datetime
from dateutil import parser
import time
import os
import cvxopt as cv
import cvxopt.solvers

windowHalf=30

directory_to_check = "/home/affectech/Desktop"# Which directory do you want to start with?


def my_function(directory):
      print("Listing: " + directory)

      try:
          EDA=pd.read_csv(directory+'/EDA_with_weather.csv')
          Frust=pd.read_csv(directory+'/Affectech_Istanbul_Frust.csv',skiprows=0)
      except:
          print("File not found exception")
          return


      EDAMatrix=EDA.ix[:,:]
      HRMatrix=Frust.ix[:,:]
      
      HrLen=len(HRMatrix)
      EDALen=len(EDAMatrix)
      
      lastStr=""
      
      for i in range(0,EDALen):
          startTimestamp=EDAMatrix.values[i][10]
          endTimestamp = EDAMatrix.values[i][11]
         
          for j in range(0,HrLen):
              startTimestampHR=HRMatrix.values[j][2]
              endTimestampHR = HRMatrix.values[j][0]
              
              if(startTimestampHR == startTimestamp and endTimestamp==endTimestampHR ):
                  segStr=str(EDAMatrix.values[i][0])+","+str(EDAMatrix.values[i][1])+","+str(EDAMatrix.values[i][2])+","+str(EDAMatrix.values[i][3])+","+str(EDAMatrix.values[i][4])+","+str(EDAMatrix.values[i][5])+","+str(EDAMatrix.values[i][6])+","+str(EDAMatrix.values[i][7])+","+str(EDAMatrix.values[i][8])+","+str(EDAMatrix.values[i][9])+","+str(EDAMatrix.values[i][10]) +","+str(EDAMatrix.values[i][11])+","+str(EDAMatrix.values[i][12])+","+str(EDAMatrix.values[i][13])+","+str(EDAMatrix.values[i][14])+","+str(EDAMatrix.values[i][15])+","+str(EDAMatrix.values[i][16])+","+str(EDAMatrix.values[i][17])+","+str(EDAMatrix.values[i][18])+","+str(EDAMatrix.values[i][19])+","+str(EDAMatrix.values[i][20])+","+str(HRMatrix.values[j][3])+","+str(HRMatrix.values[j][4])+"\n"
                  lastStr=lastStr+segStr
              


      return lastStr



# Get all the subdirectories of directory_to_check recursively and store them in a list:
directories = [os.path.abspath(x[0]) for x in os.walk(directory_to_check)]
#directories.remove(os.path.abspath(directory_to_check)) # If you don't want your main directory included

string=""
f = open('/home/affectech/Desktop/combined.csv','w')
f.write("mean"+","+"peak"+","+"perc20"+","+"perc80"+","+"QuartDev"+","+"std"+","+"strong_peak"+","+"dataQual"+","+"label_sub"+","+"session_label"+","+"session"+","+"device"+","+"temp"+","+"p0"+","+"p"+","+"p_diff"+","+"hum"+","+"wind"+","+"cloud"+","+"temp_dew"+","+"precip"+","+"frust_2"+","+"frust_3"+"\n")
for i in directories:
      os.chdir(i)         # Change working Directory
      #if(os.path.isfile(i+'/EDA.csv')==False):
      #    continue
      lineStr=my_function(i)
      if not (lineStr is None):
          f.write(lineStr)

f.close()