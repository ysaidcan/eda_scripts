#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 22:00:27 2018

@author: root
"""
import pandas as pd
import numpy as np


try:
    features = pd.read_csv('./peakProps_120.csv')
    eventLogs = pd.read_csv('./data_.csv' )
except:
    print("File not found exception")


    # parsing files into python arrays
mean = features['mean']
std = features['std']
peak = features['peak']
strong_peak = features['strong_peak']
perc20 = features['perc20']
perc80 = features['perc80']
quartDev = features['quartDev']
session = features['session']
label_subjective = features['label_subjective']
startLabels = features['startTimestamp']
endLabels = features['endTimeStamp']
max = features['max']
min = features['min']
dataQualityRatio = features['dataQualityRatio']
derivative = features['derivative']
sessionNumber = eventLogs.ix[:, 0]
sessionStart = eventLogs.ix[:, 2]
sessionEnd = eventLogs.ix[:, 3]
sessionLabel = eventLogs.ix[:, 4]
sessionLabel2 = eventLogs.ix[:, 5]
#sessionLabel3 = eventLogs.ix[:, 6]

sessionMat=np.zeros((len(startLabels),14),dtype=float)
sessionMat.fill(np.nan)
session_string= np.empty(len(startLabels),dtype="S10")
session_string.fill(np.nan)

session_label_string= np.empty(len(startLabels),dtype="S10")
session_label_string.fill(np.nan)

session_label_string2= np.empty(len(startLabels),dtype="S10")
session_label_string2.fill(np.nan)

session_label_string3= np.empty(len(startLabels),dtype="S10")
session_label_string3.fill(np.nan)


session_id_string= np.empty(len(startLabels),dtype="S10")
session_id_string.fill(np.nan)
for i in range(0, len(startLabels)):
    for j in range(0,len(eventLogs)):
        if(sessionStart[j] <= startLabels[i] <= sessionEnd[j]):
            sessionMat[i,0]=mean[i]
            sessionMat[i, 1] = std[i]
            sessionMat[i, 2] = peak[i]
            sessionMat[i,3] = strong_peak[i]
            sessionMat[i,4] = perc20[i]
            sessionMat[i,5] = perc80[i]
            sessionMat[i,6] = quartDev[i]
            session_string[i]=session[i]
            sessionMat[i,7]=label_subjective[i]
            sessionMat[i,8]=startLabels[i]
            sessionMat[i,9]=endLabels[i]
            sessionMat[i, 10] = max[i]
            sessionMat[i, 11] = min[i]
            sessionMat[i, 12] = dataQualityRatio[i]
            sessionMat[i,13] = derivative[i]
            session_id_string[i]=sessionNumber[j]
            session_label_string[i]=sessionLabel[j]
            session_label_string2[i] = sessionLabel2[j]
 #           session_label_string3[i] = sessionLabel3[j]

dataset = pd.DataFrame({'Mean':sessionMat[:,0],'Std':sessionMat[:,1],'Peak':sessionMat[:,2],'Strong_Peak':sessionMat[:,3],'Perc20':sessionMat[:,4],
'Perc80':sessionMat[:,5],'QuartDev':sessionMat[:,6],'Device':session_string,'Lab_sub':sessionMat[:,7],'Start_time':sessionMat[:,8],
'endTime':sessionMat[:,9],'dataQual':sessionMat[:,10],'Max':sessionMat[:,11],'Min':sessionMat[:,12],'derivative':sessionMat[:,13],'SessionID':session_id_string,'SessionLabel':session_label_string,'SessionLabel2':session_label_string2})


sessionMat=pd.DataFrame(sessionMat,columns=['Mean', 'Std', 'Peak', 'Strong_Peak', 'Perc20', 'Perc80', 'QuartDev', 'Lab_sub', 'Start_time', 'end_time', 'Max', 'Min','dataQual', 'derivative'])
session_id_string=pd.DataFrame(session_id_string, columns=['SessionID'])
session_label_string=pd.DataFrame(session_label_string,columns=['SessionLabel'])
session_label_string2=pd.DataFrame(session_label_string2,columns=['SessionLabel2'])
session_string=pd.DataFrame(session_string,columns=['Device'])

frame=pd.concat([sessionMat,session_id_string,session_label_string,session_label_string2,session_string],axis=1)

# Convert list of tuples to dataframe and set column names and indexes

frame.dropna(axis=0, how='any', inplace=True)

frame.to_csv('./son.csv', encoding='utf-8')












